BUILDIR		= $(CURDIR)/build
SRCDIR		= $(CURDIR)/src
BR_EXT 		= $(CURDIR)/buildroot-external
BR 			= $(CURDIR)/buildroot
BOARD		= IGEPv2

.DEFAULT_GOAL := build
.PHONY: setup config source build menuconfig %-menuconfig $(SRCDIR) $(BUILDIR)

setup: $(SRCDIR) $(BUILDIR)
	git submodule update --init
	$(MAKE) config

config: $(BUILDIR)/.config

$(SRCDIR) $(BUILDIR):
	mkdir -p $@

source: $(BUILDIR)/.config
	$(MAKE) -C $(BUILDIR) BR2_DL_DIR=$(SRCDIR) source

build: source
	$(MAKE) -C $(BUILDIR) BR2_DL_DIR=$(SRCDIR)

$(BUILDIR)/.config: $(BR_EXT)/configs/$(BOARD)_defconfig $(BR)/Makefile | $(SRCDIR) $(BUILDIR)
	$(MAKE) -C $(BR) O=$(BUILDIR) BR2_EXTERNAL=$(BR_EXT) BR2_DL_DIR=$(SRCDIR) $(BOARD)_defconfig

$(BR)/Makefile: | $(BUILDIR)
	$(MAKE) setup

menuconfig:
	$(MAKE) -C $(BUILDIR) BR2_DL_DIR=$(SRCDIR) menuconfig

%-menuconfig: $(BUILDIR)/.config
	$(MAKE) -C $(BUILDIR) BR2_DL_DIR=$(SRCDIR) $@

savedefconfig:
	$(MAKE) -C $(BUILDIR) savedefconfig

clean:
	$(MAKE) -C $(BUILDIR) clean

distclean:
	rm -rf $(BUILDIR)

scrub:
	rm -rf $(BUILDIR) $(SRCDIR)
